<?php
/**
* googleWeather module
* Displays current weather status at a module position
* @version 2.0 May 2012
* @author computer-daten-netze:feenders - dirk hoeschen
* @authorurl http://www.feenders.de
* @copyright (C) 2008-2012 by feenders.de
* @license	GNU/GPL, see http://www.gnu.org/licenses/gpl-2.0.html
*
*/

defined( '_JEXEC' ) or die( 'Direct Access not allowed.' );

// array with languages and encodings. php isnt able to detect charset
$encodings = array ('ru' => 'windows-1251','lt' => 'windows-1251','tr' => 'ISO-8859-9','ar' => 'ISO-8859-6','el' => 'ISO-8859-7','it' => 'ISO-8859-1','fr' => 'ISO-8859-1','zh'=>'GB2312');

require_once (dirname(__FILE__).DS.'helper.php');

// detect laguage if demanded
if ($params->get('use_page_language','true')) {
	require_once JPATH_SITE.DS.'libraries'.DS.'joomla'.DS.'language'.DS.'helper.php';
	$weather_language = JLanguageHelper::detectLanguage();
	$weather_language = substr($weather_language,0,2);
} else {
	$weather_language = $params->get('weather_language','en');
}

// build Google API url
$weather_url = 'http://www.google.com/ig/api?weather='.urlencode(trim($params->get('weather_location','berlin'))).'&hl='.urlencode($weather_language);
$icon_path = "modules/mod_googleWeather/icons/".$params->get( 'iconset','modern')."/";
// create output string
$output = "<div class='mod_weather".$params->get( 'moduleclass_sfx' )."'>\n";

// add automatic margin to image to beauty outputs without stylesheets
$img_margin = "5px 0";
if ($params->get('img_align')=="left") { $img_margin = "0 10px 5px 0";  }
if ($params->get('img_align')=="right") { $img_margin = "0 0 5px 10px";  }

// try to use alternate method if allow_url_fopen is disabled
if (ini_get('allow_url_fopen')!="") {
	$wxml = @file_get_contents($weather_url);
//	$wxml = $wxml[0];
} else if (function_exists('curl_exec')) { // try curl
	$wxml = modGoogleWeatherHelper::getXMLbyCurl($weather_url);
}

// parse XML - file
if ($wxml) {
	$charset = (isset($encodings[$weather_language])) ? $encodings[$weather_language] : 'ISO-8859-2';
	$wxml = mb_convert_encoding($wxml,'UTF-8', $charset);
	// use alternate method if simpleXML does not exists (PHP4.x)
	if (function_exists('simplexml_load_file')) {
 		$xml = new SimpleXMLElement($wxml);
 		$weather = $xml->weather;
 	} else {
 		require_once JPATH_ROOT.DS.'modules'.DS.'mod_googleWeather'.DS.'simpleXML'.DS.'IsterXmlSimpleXMLImpl.php';
 		$sxml = new IsterXmlSimpleXMLImpl;
 		$xml = $sxml->load_string($wxml);
 		$weather = $xml->xml_api_reply->weather;
 	}
	$current = $weather->current_conditions;

 	if ($current) {
			$output .= "<div style='display: block; overflow: hidden; padding-bottom: 5px;' class='weather_current'>\n";
			if ($params->get('show_icon')) {
				$icon = modGoogleWeatherHelper::getData($current->icon);
				if (!$params->get('use_gicon')) {
					if (empty($icon)) {
						$icon = "warning.png";
					} else {
						$icon = strrchr($icon,"/");
						$icon = substr($icon, 0 , -4).".png";
					}
					$icon = JURI::root(false).$icon_path.((file_exists(JPATH_BASE."/".$icon_path.$icon)) ? $icon : "partly_cloudy.png");
				} else $icon = "http://www.google.com".$icon;
				$output .= "<img class='weather' src='".$icon."' alt='".modGoogleWeatherHelper::getData($current->condition)."' align='".$params->get('img_align')."' style='margin:".$img_margin.";' />";
			}
 			$output .= "<b>".((modGoogleWeatherHelper::getData($current->condition)!="") ? modGoogleWeatherHelper::getData($current->condition) : "---" )."</b> ";
 			switch ($params->get('temp_unit')) {
 				case 'c':
					$output .= modGoogleWeatherHelper::getData($current->temp_c)."&nbsp;<sup>o</sup>C";
					$unit = "C";
				break;
 				case 'f':
					$output .= modGoogleWeatherHelper::getData($current->temp_f)."&nbsp;<sup>o</sup>F";
					$unit = "F";
 				break;
 				default:
 					$output .= "(".modGoogleWeatherHelper::getData($current->temp_f)."&nbsp;<sup>o</sup>F&nbsp;&bull;&nbsp;";
					$output .= modGoogleWeatherHelper::getData($current->temp_c)."&nbsp;<sup>o</sup>C)";
					$unit = false;
 			   }
 			$output .= "<br /><b>".str_replace(":",":</b>",modGoogleWeatherHelper::getData($current->humidity))."<br />";
			$output .= "<b>".str_replace(":",":</b>",modGoogleWeatherHelper::getData($current->wind_condition))."<br />";
 			$output .= "</div>\n";
			// output forecast
			if ($params->get('show_forecast',1)==1) {
				$unit_system = modGoogleWeatherHelper::getData($weather->forecast_information->unit_system);
				if (!$unit) $unit = ($unit_system=="US") ? "F" : "C";
				$flimit = 0;
				$output .= "<div class='weather_forecast'><table cellpadding='0' cellspacing='0' border='0'>";
				foreach ($weather->forecast_conditions as $val => $current) {
					$low = modGoogleWeatherHelper::getData($current->low); $high = modGoogleWeatherHelper::getData($current->high);
					// recalculate degree if nessesary
					if (($unit_system=="US") && ($unit=="C")) {
						$low = round((($low-32)*5)/9); $high = round((($high-32)*5)/9);
					} else if (($unit_system=="SI") && ($unit=="F")) {
						$low = round((($low*9)/5)+32); $high = round((($high*9)/5)+32);
					}
					$output .= "<tr><td><b>".modGoogleWeatherHelper::getData($current->day_of_week)."</b>&nbsp;</td><td float='right'>"
							.$low."&nbsp;&rArr;&nbsp;".$high."&nbsp;<sup>o</sup>".$unit
							."</td><td>&nbsp;&raquo;&nbsp;".modGoogleWeatherHelper::getData($current->condition)."&nbsp;&laquo;</td></tr>";
					$flimit++;
					if ($flimit>=3) break;
				}
				$output .= "</table></div>";
			}
	} else {
		if ($params->get('show_icon')) {
			$output .= "<img class='weather' src='".$icon_path."warning.png' alt='error' align='".$params->get('img_align')."' style='margin:".$img_margin.";' />";
		}
		$output .= "<p>Could not get weather informations for ".$params->get('weather_location')."</p>\n";
	}
} else {
	if ($params->get('show_errors','true')) {
		if ($params->get('show_icon')) {
			$output .= "<img class='plg_weather' src='".$icon_path."warning.png' alt='error' align='".$params->get('img_align')."' style='margin:".$img_margin.";' />";
		}
		$output .= "<p>".$params->get('error_msg','Unable to get weather data from Google.<br/>Service is offline.')."</p>";
 	}
}
$output .= "</div>\n";
// output weather informations
echo $output;


?>
