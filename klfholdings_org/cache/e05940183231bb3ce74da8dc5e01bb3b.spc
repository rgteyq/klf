a:3:{s:5:"child";a:1:{s:0:"";a:1:{s:3:"rss";a:1:{i:0;a:6:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:1:{s:7:"version";s:3:"2.0";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:0:"";a:1:{s:7:"channel";a:1:{i:0;a:6:{s:4:"data";s:63:"
    
    
    
    
    
    
    
    
    
    
    
    
  ";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:0:"";a:10:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:14:"Sydney Weather";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:42:"http://www.weatherzone.com.au/warnings.jsp";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:54:"Weatherzone - forecasts, satellite, BoM radar and more";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 26 Jun 2012 23:52:47 +1000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:13:"lastBuildDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 26 Jun 2012 23:52:47 +1000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"language";a:1:{i:0;a:5:{s:4:"data";s:5:"en-au";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:9:"copyright";a:1:{i:0;a:5:{s:4:"data";s:30:"Copyright  The Weather Company";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"docs";a:1:{i:0;a:5:{s:4:"data";s:31:"http://backend.userland.com/rss";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:5:"image";a:1:{i:0;a:6:{s:4:"data";s:40:"
      
      
      
      
      
    ";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:0:"";a:5:{s:3:"url";a:1:{i:0;a:5:{s:4:"data";s:69:"http://www.weatherzone.com.au/images/logos/weatherzone_logo_88x20.gif";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:11:"Weatherzone";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:29:"http://www.weatherzone.com.au";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:5:"width";a:1:{i:0;a:5:{s:4:"data";s:2:"88";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"height";a:1:{i:0;a:5:{s:4:"data";s:2:"20";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}}s:4:"item";a:3:{i:0;a:6:{s:4:"data";s:33:"
      
      
      
      
    ";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:0:"";a:4:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:41:"Current weather for Sydney at 11:38pm EST";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:817:"
        <b>Temperature:</b> 11.3&#176;C
        <img align="top" src="http://www.weatherzone.com.au/images/widgets/nav_trend_steady.gif" alt="steady"/>
        <br />
        <b>Feels like:</b> 9.8&#176;C<br />
        <b>Dew point:</b> 10.1&#176;C
        <img align="top" src="http://www.weatherzone.com.au/images/widgets/nav_trend_steady.gif" alt="steady"/>
        <br />
        <b>Relative humidity:</b> 92%<br />
        <b>Wind:</b> SSW at 13 km/h, gusting to 17 km/h
        <img align="top" src="http://www.weatherzone.com.au/images/widgets/nav_trend_down.gif" alt="falling"/>
        <br />
        <b>Rain:</b> 8.8mm since 9am<br />
        <b>Pressure:</b> 1031.5 hPa
        <img align="top" src="http://www.weatherzone.com.au/images/widgets/nav_trend_steady.gif" alt="steady"/>
        <br />
        ";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:47:"http://www.weatherzone.com.au/nsw/sydney/sydney";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:47:"http://www.weatherzone.com.au/nsw/sydney/sydney";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:4:"true";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:1;a:6:{s:4:"data";s:33:"
      
      
      
      
    ";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:0:"";a:4:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:23:"Sydney weather forecast";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:661:"
<b>Wednesday</b><br />
<img src="http://www.weatherzone.com.au/images/icons/fcast_30/possible_shower.gif"><br />
Possible shower<br />
10&#176;C - 17&#176;C<br /><br />
<b>Thursday</b><br />
<img src="http://www.weatherzone.com.au/images/icons/fcast_30/mostly_sunny.gif"><br />
Mostly sunny<br />
10&#176;C - 18&#176;C<br /><br />
<b>Friday</b><br />
<img src="http://www.weatherzone.com.au/images/icons/fcast_30/mostly_sunny.gif"><br />
Mostly sunny<br />
10&#176;C - 20&#176;C<br /><br />
<b>Saturday</b><br />
<img src="http://www.weatherzone.com.au/images/icons/fcast_30/mostly_sunny.gif"><br />
Mostly sunny<br />
9&#176;C - 18&#176;C<br /><br />
        ";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:47:"http://www.weatherzone.com.au/nsw/sydney/sydney";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:47:"http://www.weatherzone.com.au/nsw/sydney/sydney";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:4:"true";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:2;a:6:{s:4:"data";s:33:"
      
      
      
      
    ";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:0:"";a:4:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:14:"Sydney weather";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:229:"
<p><a href="http://www.weatherzone.com.au/radar.jsp?lt=radar&amp;lc=003">Wollongong weather radar</a></p>
<p><a href="http://www.weatherzone.com.au/satellite.jsp?lt=wzstate&amp;lc=NSW">Satellite cloud map</a></p>
        
      ";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:40:"http://www.weatherzone.com.au/nsw/sydney";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:47:"http://www.weatherzone.com.au/nsw/sydney/sydney";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}}}}}}}}}}}}s:7:"headers";a:8:{s:6:"server";s:67:"Apache/2.2.17 (Unix) mod_ssl/2.2.17 OpenSSL/0.9.8e-fips-rhel5 DAV/2";s:12:"content-type";s:8:"text/xml";s:14:"content-length";s:4:"3346";s:9:"x-varnish";s:21:"1812819718 1812759837";s:13:"cache-control";s:9:"max-age=1";s:7:"expires";s:29:"Tue, 26 Jun 2012 14:04:08 GMT";s:4:"date";s:29:"Tue, 26 Jun 2012 14:04:07 GMT";s:10:"connection";s:10:"keep-alive";}s:5:"build";i:20070719221955;}