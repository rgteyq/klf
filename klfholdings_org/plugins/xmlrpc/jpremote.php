<?php
/**
 * @package AkeebaNativeTools
 * @subpackage Plugins
 * @copyright Copyright (c)2006-2010 Nicholas K. Dionysopoulos / Akeeba Developers
 * @license GNU General Public License version 3, or later
 * @version $id$
 * @since 3.0
 *
 * Joomla! XMLRPC services plugin for interfacing Akeeba Backup Core/Pro with Akeeba Remote Control
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

// Do not change this; this is used by the desktop application to validate
// its compatibility with the server it's talking with.
// Note: 2.2 indicates the Akeeba Backup edition of the plugin. The JoomlaPack-enabled edition's API version is 2.1
define('JPRemoteAPIVersion','2.2'); // API 2.2 - Akeeba Remote Control 2.5

// Define the backup origin and backup tag
define('AKEEBA_BACKUP_ORIGIN','xmlrpc');

// Import Joomla! plugin functionality
jimport( 'joomla.plugin.plugin' );

/**
 * JoomlaPack Remoting Services XMLRPC plugin class
 */
class plgXMLRPCJPRemote extends JPlugin
{
	function plgXMLRPCJPRemote(&$subject, $config)
	{
		parent::__construct($subject, $config);
	}

	function onGetWebServices()
	{
		// Get the global XML-RPC types
		global $xmlrpcI4, $xmlrpcInt, $xmlrpcBoolean, $xmlrpcDouble, $xmlrpcString,
		$xmlrpcDateTime, $xmlrpcBase64, $xmlrpcArray, $xmlrpcStruct, $xmlrpcValue;

		// Return the services definitions
		return array(
			'joomlapack.getAPI' => array
		(
				'function' => 'plgXMLRPCJPRemoteServices::getAPI',
				'docstring' => 'Gets the JoomlaPack Remoting Services API version',
				'signature' => array(array($xmlrpcString))
		),

			'joomlapack.getProfiles' => array
		(
				'function' => 'plgXMLRPCJPRemoteServices::getProfiles',
				'docstring' => 'Retrieves a list of valid backup profile ID\'s and their descriptions.',
				'signature' => array(array($xmlrpcArray, $xmlrpcString, $xmlrpcString))
		),

			'joomlapack.getBackups' => array
		(
				'function' => 'plgXMLRPCJPRemoteServices::getBackups',
				'docstring' => 'Retrieves a list of all backup attempts.',
				'signature' => array(array($xmlrpcArray, $xmlrpcString, $xmlrpcString))
		),

			'joomlapack.startBackup' => array
		(
				'function' => 'plgXMLRPCJPRemoteServices::startBackup',
				'docstring' => 'Starts a new backup attempt.',
				'signature' => array(
					array($xmlrpcStruct, $xmlrpcString, $xmlrpcString, $xmlrpcInt, $xmlrpcString, $xmlrpcString),
					array($xmlrpcStruct, $xmlrpcString, $xmlrpcString, $xmlrpcInt, $xmlrpcString),
					array($xmlrpcStruct, $xmlrpcString, $xmlrpcString, $xmlrpcInt),
					array($xmlrpcStruct, $xmlrpcString, $xmlrpcString)
				)
		),

			'joomlapack.continueBackup' => array
		(
				'function' => 'plgXMLRPCJPRemoteServices::continueBackup',
				'docstring' => 'Continues an already started backup attempt.',
				'signature' => array(
					array($xmlrpcStruct, $xmlrpcString, $xmlrpcString, $xmlrpcInt),
					array($xmlrpcStruct, $xmlrpcString, $xmlrpcString)
				)
		),

			'joomlapack.getFilename' => array
		(
				'function' => 'plgXMLRPCJPRemoteServices::getFilename',
				'docstring' => 'Gets the backup archive filename.',
				'signature' => array(
					array($xmlrpcString, $xmlrpcString, $xmlrpcString, $xmlrpcInt),
					array($xmlrpcString, $xmlrpcString, $xmlrpcString)
				)
		),

		/* API Version 2.0 features */

			'joomlapack.deleteFile' => array
			(
				'function' => 'plgXMLRPCJPRemoteServices::deleteFile',
				'docstring' => 'Deletes the backup archive file of a given backup record.',
				'signature' => array(
					array($xmlrpcStruct, $xmlrpcString, $xmlrpcString, $xmlrpcInt),
				)
			),

		/* API Version 2.1 features */
			'joomlapack.getParts' => array
		(
			'function' => 'plgXMLRPCJPRemoteServices::getParts',
			'docstring' => 'Gets the names of all the files this backup consists of.',
			'signature' => array(
					array($xmlrpcArray, $xmlrpcString, $xmlrpcString, $xmlrpcInt, $xmlrpcInt),
					array($xmlrpcArray, $xmlrpcString, $xmlrpcString, $xmlrpcInt),
					array($xmlrpcArray, $xmlrpcString, $xmlrpcString)
			)
		)
		);
	}
}

/**
 * This class implements the web services part of this plugin
 * @author sledge81
 *
 */
class plgXMLRPCJPRemoteServices
{
	/**
	 * Returns the implemented JoomlaPack Remoting Services API version
	 * @return string;
	 */
	function getAPI()
	{
		return JPRemoteAPIVersion;
	}

	/**
	 * Returns a list of backup profile ID's and descriptions
	 *
	 * @param string $username A valid Joomla! user's username
	 * @param string $password The user's password
	 * @return unknown_type
	 */
	function getProfiles($username, $password)
	{
		// Get the global XML-RPC types
		global $xmlrpcI4, $xmlrpcInt, $xmlrpcBoolean, $xmlrpcDouble, $xmlrpcString,
		$xmlrpcDateTime, $xmlrpcBase64, $xmlrpcArray, $xmlrpcStruct, $xmlrpcValue,
		$xmlrpcerruser;

		// Load JoomlaPack base classes
		plgXMLRPCJPRemoteHelper::AkeebaBackupBootstrap();

		// Authenticate user
		if(!plgXMLRPCJPRemoteHelper::authenticate($username, $password))
		return new xmlrpcresp(0, $xmlrpcerruser+1, JText::_("Login Failed"));

		// Do the job
		require_once JPATH_COMPONENT_ADMINISTRATOR.DS.'models'.DS.'profiles.php';
		$profilesModel = new AkeebaModelProfiles();
		$profilesList = $profilesModel->getProfilesList(true);

		$structarray = array();
		foreach($profilesList as $profile)
		{
			$aProfile = new xmlrpcval(array(
				'id' => new xmlrpcval($profile->id, $xmlrpcInt),
				'description' => new xmlrpcval($profile->description, $xmlrpcString)
			), 'struct');
			$structarray[] = $aProfile;
		}
		return new xmlrpcresp(new xmlrpcval( $structarray , $xmlrpcArray));
	}

	function getBackups($username, $password)
	{
		// Get the global XML-RPC types
		global $xmlrpcI4, $xmlrpcInt, $xmlrpcBoolean, $xmlrpcDouble, $xmlrpcString,
		$xmlrpcDateTime, $xmlrpcBase64, $xmlrpcArray, $xmlrpcStruct, $xmlrpcValue,
		$xmlrpcerruser;

		// Load base classes
		plgXMLRPCJPRemoteHelper::AkeebaBackupBootstrap();

		// Authenticate user
		if(!plgXMLRPCJPRemoteHelper::authenticate($username, $password))
		return new xmlrpcresp(0, $xmlrpcerruser+1, JText::_("Login Failed"));

		// Do the job
		require_once JPATH_COMPONENT_ADMINISTRATOR.DS.'models'.DS.'statistics.php';
		$model = new AkeebaModelStatistics();
		$allEntries =& $model->getStatisticsListWithMeta(true);

		$structarray = array();
		foreach($allEntries as $entry)
		{
			if(empty($entry)) continue;
			$aProfile = new xmlrpcval(array(
				'id' => new xmlrpcval($entry['id'], $xmlrpcInt),
				'description' => new xmlrpcval( empty($entry['description'])?'n/a':$entry['description'] , $xmlrpcString),
				'comment' => new xmlrpcval($entry['comment'], $xmlrpcString),
				'backupstart' => new xmlrpcval($entry['backupstart'], $xmlrpcString),
				'backupend' => new xmlrpcval($entry['backupend'], $xmlrpcString),
				'status' => new xmlrpcval($entry['status'], $xmlrpcString),
				'origin' => new xmlrpcval($entry['origin'], $xmlrpcString),
				'type' => new xmlrpcval(empty($entry['type'])?'xmlrpc':$entry['type'], $xmlrpcString),
				'meta' => new xmlrpcval($entry['meta'], $xmlrpcString),
				'profile' => new xmlrpcval($entry['profile_id'], $xmlrpcInt),
				'archivename' => new xmlrpcval($entry['archivename'], $xmlrpcString)
			), 'struct');
			$structarray[] = $aProfile;
		}
		return new xmlrpcresp(new xmlrpcval( $structarray , $xmlrpcArray));
	}

	function startBackup($username, $password, $profileid = 1, $description = null, $comment = null)
	{
		// Get the global XML-RPC types
		global $xmlrpcI4, $xmlrpcInt, $xmlrpcBoolean, $xmlrpcDouble, $xmlrpcString,
		$xmlrpcDateTime, $xmlrpcBase64, $xmlrpcArray, $xmlrpcStruct, $xmlrpcValue,
		$xmlrpcerruser;

		// Load base classes
		plgXMLRPCJPRemoteHelper::AkeebaBackupBootstrap();

		// Authenticate user
		if(!plgXMLRPCJPRemoteHelper::authenticate($username, $password))
		return new xmlrpcresp(0, $xmlrpcerruser+1, JText::_("Login Failed"));

		// Make sure we have a profile set throughout the XMLRPC session
		$session =& JFactory::getSession();
		$session->set('profile', $profileid, 'akeeba');
		// Load the correct registry
		AEPlatform::load_configuration($profileid);

		// Validate backup profile ID
		require_once JPATH_COMPONENT_ADMINISTRATOR.DS.'models'.DS.'profiles.php';
		$profilesModel = new AkeebaModelProfiles();
		$profilesModel->setId($profileid);
		$profile =& $profilesModel->getProfile();
		if(!is_object($profile))
		return new xmlrpcresp(0, $xmlrpcerruser+2, 'Invalid profile ID');

		// Get defaults for any missing fields
		if(is_null($description)) $description = 'XMLRPC remote backup';
		if(is_null($comment)) $comment = '';

		// Cleanup after any previous backup attempts
		AECoreKettenrad::reset();

		// Start the backup
		$kettenrad =& AECoreKettenrad::load();
		$options = array(
			'description'	=> $description,
			'comment'		=> ''
		);
		$kettenrad->setup($options); // Just passes the data
		$kettenrad->tick(); // Runs _prepare() which simply initializes the object
		$kettenrad->tick(); // And this executes the first steps and inits the stats record
		AECoreKettenrad::save();
		$ret = $kettenrad->getStatusArray();

		return new xmlrpcresp(new xmlrpcval( array(
			'IsFinished'	=> new xmlrpcval( $ret['HasRun'] != 0, $xmlrpcBoolean ),
			'Domain'		=> new xmlrpcval( $ret['Domain'], $xmlrpcString ),
			'Step'			=> new xmlrpcval( $ret['Step'], $xmlrpcString ),
			'Substep'		=> new xmlrpcval( $ret['Substep'], $xmlrpcString ),
			'Error'			=> new xmlrpcval( $ret['Error'], $xmlrpcString ),
			'Warnings'		=> new xmlrpcval( is_array($ret['Warnings']) ? implode("\n",$ret['Warnings']) : '', $xmlrpcString )
		) , $xmlrpcStruct));
	}

	function continueBackup($username, $password, $profileid = 1)
	{
		// Get the global XML-RPC types
		global $xmlrpcI4, $xmlrpcInt, $xmlrpcBoolean, $xmlrpcDouble, $xmlrpcString,
		$xmlrpcDateTime, $xmlrpcBase64, $xmlrpcArray, $xmlrpcStruct, $xmlrpcValue,
		$xmlrpcerruser;

		// Load base classes
		plgXMLRPCJPRemoteHelper::AkeebaBackupBootstrap();

		// Authenticate user
		if(!plgXMLRPCJPRemoteHelper::authenticate($username, $password))
		return new xmlrpcresp(0, $xmlrpcerruser+1, JText::_("Login Failed"));

		// Make sure we have a profile set throughout the XMLRPC session
		$session =& JFactory::getSession();
		$session->set('profile', $profileid, 'akeeba');
		// Load the correct registry
		AEPlatform::load_configuration($profileid);

		// Validate backup profile ID
		require_once JPATH_COMPONENT_ADMINISTRATOR.DS.'models'.DS.'profiles.php';
		$profilesModel = new AkeebaModelProfiles();
		$profilesModel->setId($profileid);
		$profile =& $profilesModel->getProfile();
		if(!is_object($profile))
		return new xmlrpcresp(0, $xmlrpcerruser+2, 'Invalid profile ID');

		// Load Kettenrad and continue the backup
		$kettenrad =& AECoreKettenrad::load();
		$kettenrad->tick();
		AECoreKettenrad::save();
		$ret = $kettenrad->getStatusArray();

		return new xmlrpcresp(new xmlrpcval( array(
			'IsFinished'	=> new xmlrpcval( $ret['HasRun'] != 0, $xmlrpcBoolean ),
			'Domain'		=> new xmlrpcval( $ret['Domain'], $xmlrpcString ),
			'Step'			=> new xmlrpcval( $ret['Step'], $xmlrpcString ),
			'Substep'		=> new xmlrpcval( $ret['Substep'], $xmlrpcString ),
			'Error'			=> new xmlrpcval( $ret['Error'], $xmlrpcString ),
			'Warnings'		=> new xmlrpcval( is_array($ret['Warnings']) ? implode("\n",$ret['Warnings']) : '', $xmlrpcString )
		) , $xmlrpcStruct));
	}

	function getFilename($username, $password, $profileid)
	{
		// Get the global XML-RPC types
		global $xmlrpcI4, $xmlrpcInt, $xmlrpcBoolean, $xmlrpcDouble, $xmlrpcString,
		$xmlrpcDateTime, $xmlrpcBase64, $xmlrpcArray, $xmlrpcStruct, $xmlrpcValue,
		$xmlrpcerruser;

		// Load base classes
		plgXMLRPCJPRemoteHelper::AkeebaBackupBootstrap();

		// Authenticate user
		if(!plgXMLRPCJPRemoteHelper::authenticate($username, $password))
		return new xmlrpcresp(0, $xmlrpcerruser+1, JText::_("Login Failed"));

		// Make sure we have a profile set throughout the XMLRPC session
		$session =& JFactory::getSession();
		$session->set('profile', $profileid, 'akeeba');
		// Load the correct registry
		AEPlatform::load_configuration($profileid);

		// Validate backup profile ID
		require_once JPATH_COMPONENT_ADMINISTRATOR.DS.'models'.DS.'profiles.php';
		$profilesModel = new AkeebaModelProfiles();
		$profilesModel->setId($profileid);
		$profile =& $profilesModel->getProfile();
		if(!is_object($profile))
		return new xmlrpcresp(0, $xmlrpcerruser+2, 'Invalid profile ID');

		// Load Kettenrad and get the filename
		$kettenrad =& AECoreKettenrad::load();
		$statistics =& AEFactory::getStatistics();
		$statRecord = $statistics->getRecord();
		$filename = $statRecord['archivename'];

		return new xmlrpcresp(new xmlrpcval( $filename, $xmlrpcString ));
	}

	function deleteFile($username, $password, $backupid)
	{
		// Get the global XML-RPC types
		global $xmlrpcI4, $xmlrpcInt, $xmlrpcBoolean, $xmlrpcDouble, $xmlrpcString,
		$xmlrpcDateTime, $xmlrpcBase64, $xmlrpcArray, $xmlrpcStruct, $xmlrpcValue,
		$xmlrpcerruser;

		// Load base classes
		plgXMLRPCJPRemoteHelper::AkeebaBackupBootstrap();

		// Authenticate user
		if(!plgXMLRPCJPRemoteHelper::authenticate($username, $password))
		return new xmlrpcresp(0, $xmlrpcerruser+1, JText::_("Login Failed"));

		// Validate backup record ID
		$statistic =& AEPlatform::get_statistics($backupid);
		if(empty($statistic))
		return new xmlrpcresp(0, $xmlrpcerruser+2, 'Invalid backup record ID');

		// Try to remove the file
		require_once JPATH_COMPONENT_ADMINISTRATOR.DS.'models'.DS.'statistics.php';
		$statisticsModel = new AkeebaModelStatistics();
		if(!$statisticsModel->removeFile($backupid))
		{
			return new xmlrpcresp(0, $xmlrpcerruser+3, 'Error while deleting file: '.$statisticsModel->getError());
		}

		return new xmlrpcresp(new xmlrpcval( $filename, $xmlrpcString ));
	}

	function getParts($username, $password, $profileid = 1, $backupid = null)
	{
		// Get the global XML-RPC types
		global $xmlrpcI4, $xmlrpcInt, $xmlrpcBoolean, $xmlrpcDouble, $xmlrpcString,
		$xmlrpcDateTime, $xmlrpcBase64, $xmlrpcArray, $xmlrpcStruct, $xmlrpcValue,
		$xmlrpcerruser;

		// Load base classes
		plgXMLRPCJPRemoteHelper::AkeebaBackupBootstrap();

		// Authenticate user
		if(!plgXMLRPCJPRemoteHelper::authenticate($username, $password))
		return new xmlrpcresp(0, $xmlrpcerruser+1, JText::_("Login Failed"));

		// Validate backup record ID

		if(empty($backupid))
		{
			$list = AEPlatform::get_statistics_list(0,1,$profileid);
			$backupid = $list[0]['id'];
		}

		$statistic =& AEPlatform::get_statistics($backupid);
		if(empty($statistic))
		return new xmlrpcresp(0, $xmlrpcerruser+2, 'Invalid backup record ID');


		$allFilenames = AEUtilStatistics::get_all_filenames($statistic);

		$ret = array();
		if(count($allFilenames) > 0)
		{
			foreach($allFilenames as $filename)
			{
				$ret[] = new xmlrpcval( basename($filename) , $xmlrpcString);
			}
		}

		return new xmlrpcresp(new xmlrpcval( $ret, $xmlrpcArray ));
	}
}

/**
 * This is an internal class of utility functions
 *
 */
class plgXMLRPCJPRemoteHelper
{
	/**
	 * Defines path constants which are not defined in XMLRPC mode and pre-loads the
	 * Akeeba Backup base classes.
	 */
	public function AkeebaBackupBootstrap()
	{
		// Populate missing defines in XMLRPC mode
		if(!defined('JPATH_COMPONENT_ADMINISTRATOR'))
		{
			define('JPATH_COMPONENT_ADMINISTRATOR', JPATH_ADMINISTRATOR.DS.'components'.DS.'com_akeeba' );
			define('AKEEBAENGINE', 1); // Required for accessing Akeeba Engine's factory class
			define('AKEEBAPLATFORM', 'joomla15'); // So that platform-specific stuff can get done!
		}

		// Awful hack: Get the front-end default language by querying the database
		$systemlang = '';
		$db =& JFactory::getDBO();
		$sql = 'SELECT '.$db->nameQuote('params').' FROM '.$db->nameQuote('#__components').
			' WHERE '.$db->nameQuote('option').' = '.$db->Quote('com_languages');
		$db->setQuery($sql);
		$db->query();
		if($db->getNumRows() > 0)
		{
			$paramstring = $db->loadResult();
			$pairs = explode("\n", $paramstring);
			if(count($pairs) > 0)
			{
				foreach($pairs as $string)
				{
					$temp = explode('=', $string, 2);
					if(count($temp) == 2)
					{
						if($temp[0] == 'site') $systemlang = $temp[1];
					}
				}
			}
		}

		// Make sure we have a profile set throughout the component's lifetime
		$session =& JFactory::getSession();
		$profile_id = $session->get('profile', null, 'akeeba');
		if(is_null($profile_id))
		{
			// No profile is set in the session; use default profile
			$session->set('profile', 1, 'akeeba');
		}

		// Get the languages to be loaded
		$jlang =& JFactory::getLanguage();

		// Load the factory
		jimport('joomla.filesystem.file');
		require_once JPATH_COMPONENT_ADMINISTRATOR.DS.'akeeba'.DS.'factory.php';
		// Load the Akeeba Backup configuration and check user access permission
		$registry =& AEFactory::getConfiguration();
		AEPlatform::load_configuration();
		unset($registry);

		require_once JPATH_COMPONENT_ADMINISTRATOR.DS.'helpers'.DS.'includes.php';

		// Frontend language load order: English, system default
		$jlang->load('com_akeeba', JPATH_ROOT, 'en-GB', true);
		$jlang->load('com_akeeba', JPATH_ROOT, $systemlang, true);

		$jlang->load('com_akeeba', JPATH_ADMINISTRATOR, 'en-GB', true);
		$jlang->load('com_akeeba', JPATH_ADMINISTRATOR, $systemlang, true);

		// Load the utils helper library
		AEPlatform::load_version_defines();

		// If JSON functions don't exist, load our compatibility layer
		if( (!function_exists('json_encode')) || (!function_exists('json_decode')) )
		{
			require_once JPATH_COMPONENT_ADMINISTRATOR.DS.'helpers'.DS.'jsonlib.php';
		}

	}


	/**
	 * Checks if a username/password pair is a valid login for this Joomla! installation
	 * and has adequate permissions to use Akeeba Backup.
	 *
	 * @param string $username Username
	 * @param string $password Password
	 * @return bool True if it is a valid login AND has enought permissions to use JoomlaPack
	 */
	public function authenticate($username, $password)
	{
		jimport('joomla.application.component.helper');
		$component =& JComponentHelper::getComponent( 'com_akeeba' );
		$params = new JParameter($component->params);

		// Get frontend backup enabled status and the secret word
		$febEnabled = $params->get('frontend_enable',0) != 0;
		$secret_word=$params->get('frontend_secret_word','');
		$secret_word = trim($secret_word);

		// -- Try to authenticate against secret word
		// Is front-end backup enabled?
		if($febEnabled)
		{
			// Only check if the secret word is not an empty string
			if( !empty($secret_word) )
			{
				// If the password matches the secret word then return possitive result and quit
				if($secret_word == $password) return true;
			}
		}

		// ********************************************************************
		// This code segment is executed if and only if we didn't have a secret
		// word match in the code above. It might fail if the site has some
		// extremely badly written user or authentication plug-ins installed.
		// ********************************************************************

		// Forcibly use Joomla! Authentication plugin, even if it's not published
		jimport( 'joomla.user.authentication');
		JLoader::import( 'authentication.joomla', JPATH_ROOT.DS.'plugins', 'plugins' );

		$jauth =& JAuthentication::getInstance();
		$auth = new plgAuthenticationJoomla($jauth, array());
		$response = new JAuthenticationResponse();
		$credentials = array( 'username' => $username, 'password' => $password );
		$options = array();
		$auth->onAuthenticate($credentials, $options, $response);

		// CATCH 1: Return false on invalid authentication
		if( !($response->status === JAUTHENTICATE_STATUS_SUCCESS) )
		return false;

		// Login the user
		$user =& JFactory::getUser($username);
		plgXMLRPCJPRemoteHelper::getUserAid($user);

		// Compare user's group to JoomlaPack's minimum privileges setting
		$authlevel = 24; // Default authlevel = Administrators & Super Administrators only
		$gid = $user->gid;
		switch($authlevel)
		{
			case 25:
				// Super administrator access only

				if($gid != 25)
				{
					return false;
				}
				break;

			case 24:
				// Administrator access allowed
				if( ($gid != 25) && ($gid != 24) )
				{
					return false;
				}
				break;

			case 23:
				// Managers access allowed
				if( ($gid != 25) && ($gid != 24) && ($gid != 23) )
				{
					return false;
				}
				break;
		}

		// All checks passed; you are authenticated
		return true;
	}

	/**
	 * Performs user login so that everything in JUser works
	 */
	function getUserAid( &$user )
	{
		$acl = &JFactory::getACL();

		//Get the user group from the ACL
		$grp = $acl->getAroGroup($user->get('id'));

		// Mark the user as logged in
		$user->set('guest', 0);
		$user->set('aid', 1);

		// Fudge Authors, Editors, Publishers and Super Administrators into the special access group
		if ($acl->is_group_child_of($grp->name, 'Registered')      ||
		$acl->is_group_child_of($grp->name, 'Public Backend')) {
			$user->set('aid', 2);
		}
	}

}