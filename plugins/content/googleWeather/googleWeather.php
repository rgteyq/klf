<?php
/**
*
* googleWeather plugin
* Displays a weather status inside Joomla content item or article
* @version 2.0 for Joomla 1.5 to 2.5 may 2012
* @author computer-daten-netze:feenders - dirk hoeschen
* @authorurl http://www.feenders.de
* @copyright (C) 2008-2012 by feenders.de
* @license	GNU/GPL, see http://www.gnu.org/licenses/gpl-2.0.html
*
*/

defined( '_JEXEC' ) or die( 'Direct Access not allowed.' );

jimport('joomla.plugin.plugin');

class plgContentgoogleWeather extends JPlugin {

	protected $icon_path = null;

	/**
	 * Plugin for backward compatibility to 1.5
	 * @param	mixed	An object with a "text" property or the string to be cloaked.
	 * @param	array	Additional parameters.
	 * @param	int		Optional page number. Unused. Defaults to zero.
	 * @return	boolean	True on success.
	 */
	public function onPrepareContent( &$row, &$params, $limitstart=0 ) {
		$plugin =& JPluginHelper::getPlugin('content', 'googleWeather');
		$this->params = new JParameter( $plugin->params );
		$this->icon_path = "plugins/content/icons/".$this->params->get('iconset','modern')."/";
		$this->_getWeather($row, $params);
		return true;
	}

	/**
	 * Plugin that replaces {weather city,county} with a weather info box Joomla1.6
	 *
	 * @param	string	The context of the content being passed to the plugin.
	 * @param	mixed	An object with a "text" property or the string to be cloaked.
	 * @param	array	Additional parameters.
	 * @param	int		Optional page number. Unused. Defaults to zero.
	 * @return	boolean	True on success.
	 */
	public function onContentPrepare($context, &$row, &$params, $limitstart=0 ) {
		$this->icon_path = "plugins/content/googleWeather/icons/".$this->params->get('iconset','modern')."/";
		$this->_getWeather($row, $params);
		return true;
	}

	protected function _getWeather(&$row, &$params ) {
		// find tags in content-text
		preg_match_all('/\{weather (.*?)\}/',$row->text, $matches);
		if ($matches) {
			// detect laguage if demanded
			if ($this->params->get('use_page_language','true')) {
				if (isset($row->language) && $row->language!="") { // get content-page language
					$this->params->weather_language = $row->language;
				}
				if (!isset($this->params->weather_language)) { // get browser language
					require_once JPATH_SITE.DS.'libraries'.DS.'joomla'.DS.'language'.DS.'helper.php';
					$this->params->weather_language = JLanguageHelper::detectLanguage();
				}
				$this->params->weather_language = substr($this->params->weather_language,0,2);
			} else {
				$this->params->weather_language = $this->params->get('weather_language','en');
			}

			// add automatic margin to image to beauty outputs without stylesheets
			$this->params->img_align = $this->params->get('img_align','left');
			$img_margin = "margin-bottom";
			if ($this->params->img_align=="left") { $img_margin = "margin-right";  }
			if ($this->params->img_align=="right") { $img_margin = "margin-left";  }
		}

		foreach($matches[1] as $wparam )
		{
			$pa = preg_split("/:/",$wparam);
			$location = $pa[0];
			$xtrap = (isset($pa[1])) ? $pa[1] : false;
			// build Google API url
			$weather_url = 'http://www.google.com/ig/api?weather='.urlencode(trim($location)).'&hl='.urlencode($this->params->weather_language);
			$output = "<div class='plg_weather'>";
			// use alternate method if allow_url_fopen
			if (ini_get('allow_url_fopen')!="") {
				$wxml = @file_get_contents($weather_url);
			} else if (function_exists('curl_exec')) { // try curl
				$ch = curl_init();
				$header = array();
				$header[0] = "Accept: text/xml,application/xml,application/xhtml+xml,";
				$header[0].= "text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5";
				$header[] = "Cache-Control: max-age=0";
				$header[] = "Connection: keep-alive";
				$header[] = "Keep-Alive: 300";
				$header[] = "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7";
				$header[] = "Accept-Language: en-us,en;q=0.5";
				curl_setopt($ch, CURLOPT_URL, $weather_url);
				curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
				curl_setopt($ch, CURLOPT_TIMEOUT, 180);
	       			curl_setopt($ch, CURLOPT_HEADER, 0);
       				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				$wxml = curl_exec($ch);
				curl_close($ch);
			}

			// array with languages and encodings. php isnt able to detect charset
			$encodings = array ('ru' => 'windows-1251','lt' => 'windows-1251','tr' => 'ISO-8859-9','ar' => 'ISO-8859-6','el' => 'ISO-8859-7','it' => 'ISO-8859-1','fr' => 'ISO-8859-1','zh'=>'GB2312');

			// parse XML - file
 			if ($wxml) {
				$charset = (isset($encodings[$this->params->weather_language])) ? $encodings[$this->params->weather_language] : 'ISO-8859-2';
				$wxml = mb_convert_encoding($wxml,'UTF-8', $charset);
 				// use alternate method if simpleXML does not exists
 				if (function_exists('simplexml_load_file')) {
 					$xml = new SimpleXMLElement($wxml);
 					$weather = $xml->weather;
 				} else {
 					require_once JPATH_ROOT."/plugins/content/googleWeather/simpleXML/IsterXmlSimpleXMLImpl.php";
 					$sxml = new IsterXmlSimpleXMLImpl;
 					$xml = $sxml->load_string($wxml);
 					$weather = $xml->xml_api_reply->weather;
 				}
 				$current = $weather->current_conditions;
 				if ($current) {
 					$output .= "<div style='display: block;overflow: hidden; padding: 5px 0;' class='weather_current'>";
	 				// create output string
					if ($this->params->get('show_icon')) {
						$icon = $this->getData($current->icon);
						if (!$this->params->get('use_gicon')) {
							if (empty($icon)) {
								$icon = "warning.png";
							} else {
								$icon = strrchr($icon,"/");
								$icon = substr($icon, 0 , -4).".png";
							}
							$icon = JURI::root(false).$this->icon_path.((file_exists(JPATH_BASE."/".$this->icon_path.$icon)) ? $icon : "partly_cloudy.png");
						} else $icon = "http://www.google.com".$icon;
						$output .= "<img class='weather' src='".$icon."' alt='".$this->getData($current->condition)."' align='".$this->params->img_align."' style='".$img_margin.": 10px;' />";
					}
 					$output .= "<b>".(($this->getData($current->condition)!="") ? $this->getData($current->condition) : "---" )."</b> ";
 					switch ($this->params->get('temp_unit')) {
 						case 'c':
							$output .= $this->getData($current->temp_c)."&nbsp;<sup>o</sup>C";
							$unit = "C";
						break;
 						case 'f':
							$output .= $this->getData($current->temp_f)."&nbsp;<sup>o</sup>F";
							$unit = "F";
						break;
 						default:
							$output .= "(".$this->getData($current->temp_f)."&nbsp;<sup>o</sup>F&nbsp;&bull;&nbsp;";
							$output .= $this->getData($current->temp_c)."&nbsp;<sup>o</sup>C)";
							$unit = false;
 				    }
					$output .= "<br /><b>".str_replace(":",":</b>",$this->getData($current->humidity))."<br />";
					$output .= "<b>".str_replace(":",":</b>",$this->getData($current->wind_condition))."<br />";
 					$output .= "</div>";
					// output forcast
					if (($xtrap=="forecast") || ($this->params->get('show_forecast')==1)) {
						// get the language specific unit system
						$unit_system = $this->getData($weather->forecast_information->unit_system);
						if (!$unit) $unit = ($unit_system=="US") ? "F" : "C";
						$flimit = 0;
						$output .= "<div class='weather_forecast'><table cellpadding='0' cellspacing='0' border='0'>";
						foreach ($weather->forecast_conditions as $val => $current) {
							$low = $this->getData($current->low); $high = $this->getData($current->high);
							// recalculate degree if nessesary
							if (($unit_system=="US") && ($unit=="C")) {
								$low = round((($low-32)*5)/9); $high = round((($high-32)*5)/9);
							} else if (($unit_system=="SI") && ($unit=="F")) {
								$low = round((($low*9)/5)+32); $high = round((($high*9)/5)+32);
							}
							$output .= "<tr><td><b>".$this->getData($current->day_of_week)."</b>&nbsp;</td><td float='right'>"
									.$low."&nbsp;&rArr;&nbsp;".$high."&nbsp;<sup>o</sup>".$unit
									."</td><td>&nbsp;&raquo;&nbsp;".$this->getData($current->condition)."&nbsp;&laquo;</td></tr>";
							$flimit++;
							if ($flimit>=3) break;
						}
						$output .= "</table></div>";
					}

 				} else {
					if ($this->params->get('show_icon')) {
 						$output .= "<img class='weather' src='".$this->icon_path."warning.png' alt='error' align='".$this->params->img_align."' style='".$img_margin.": 10px;' />";
					}
 					$output .= "<p>Could not retrieve weather informations for ".$location."</p>";
 				}
 			} else {
 				if ($this->params->get('show_errors','true')) {
					if ($this->params->get('show_icon')) {
 						$output .= "<img class='plg_weather' src='".$this->icon_path."warning.png' alt='error' align='".$this->params->img_align."' style='".$img_margin.": 10px;' />";
					}
 					$output .= "<p>".$this->params->get('error_msg','Unable to get weather data from Google.<br/>Service is offline.')."</p>";
 				}
 			}
			$output .= "</div>";
			// replace tag with code
			$row->text = str_replace("{weather $wparam}", $output, $row->text);
		}
		return true;
	}

	// a bit complicate but if the simpleXML replacement is not 100% compatible
	protected function getData( $node ) {
		if ($node) {
			$data = $node->attributes();
			return $data['data'];
		}
	}

}

?>
